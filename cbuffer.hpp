#ifndef CBUFFER_HPP
#define CBUFFER_HPP

#include <iostream>
#include <algorithm>
#include <cassert>
#include <stdexcept>
#include <iterator>
#include <cstddef> 

/**
   @file cbuffer.hpp
   @brief Dichiarazione della classe cbuffer
*/

/**
   Classe che implementa un buffer circolare di dimensione fissa
   decisa nel momento di istanziazione dell'oggetto. L'elemento più
   vecchio, quello inserito meno recentemente, sarà considerato essere
   sempre nella posizione 0 del buffer circolare. Si potrà accedere
   solo agli elementi in posizioni in cui ci sono elementi validi. Gli
   accessi con indici superiori al numero di elementi usati non
   permetteranno di effettuare "il giro" del buffer ripartendo
   dall'inizio. 
   @brief Buffer circolare
   @param T tipo del dato
*/
template <typename T>
class cbuffer{
public:
  typedef T value_type;
  typedef unsigned int size_type; ///< Definizione del tipo corrispondente a size
  
private:
  value_type* _buffer; ///< Puntatore all'array di value_type (T) 
  size_type _start; ///< Indice di dove inizia il cbuffer rispetto all'array fisico
  size_type _end; ///< Indice che indica dove va inserito nell'array fisico il prossimo elemento da inserire
  size_type _size; ///< Dimensione del cbuffer
  size_type _used; ///< Quanti elementi sono effettivamente presenti nel cbuffer 

  /**
     Metodo privato di appoggio, elimina e setta tutte le variabili di
     un cbuffer a zero e quindi lo rende inutilizzabile
  */
  void clear(){
    delete[] this -> _buffer;
    this -> _buffer = 0;
    this -> _size = 0;
    this -> _start = 0;
    this -> _end = 0;
    this -> _used = 0;
  }
  
public:
  //METODI FONDAMENTALI

  /**
     Costruttore di default per istanziare un cbuffer vuoto. Si noti
     che se si prova ad utilizzare un cbuffer costruito con questo
     costruttore si avranno problemi in fase di esecuzione poichè il
     puntatore è a 0.
     @brief Costruttore di default
  */
  cbuffer(): _buffer(0), _size(0), _start(0), _end(0), _used(0){
#ifndef NDEBUG
    std::cout << "cbuffer()" << std::endl;
#endif
  }

  /**
     Costruttore di copia. Permette di istanziare un cbuffer con i
     valori presi da un altro cbuffer. Se viene lanciata un'eccezione
     metterà il cbuffer in stato coerente, uno vuoto, e rilancerà
     l'eccezione 
     @brief Copy constructor
     @param other cbuffer da usare per creare quello corrente
     @throw Eccezione di allocazione di memoria
  */
  cbuffer(const cbuffer &other): _buffer(0), _size(0), _start(0),
				 _end(0), _used(0){
    this -> _buffer = new T[other._size];
    this -> _size = other._size;
    this -> _start = other._start;
    this -> _end = other._end;
    this -> _used = other._used;
    if(!is_empty()){
      try{
	if(!is_full()){
	  //diverso perchè tanto sono in modulo e ci passerò solo una
	  //volta da _end che è quando dovrò fermarmi
	  for(size_type i = _start; i != _end; i = (++i)%_size){
	    this -> _buffer[i] = other._buffer[i];
	  }
	}
	else{
	  //se è pieno mi basta ciclare su tutto il _buffer da 0 a _size
	  //poichè start ed end sono già stati assegnati questo modo
	  //di assegnare non modificherà il cbuffer rispetto a quello
	  //originario 
	  for(size_type i = 0; i < _size; ++i){
	    this -> _buffer[i] = other._buffer[i];
	  }
	}
      }
      catch(...){
	clear();
	throw;
      }
    }
    // se è vuoto non devo fare nulla neanche inizializzare T a dei
    // valori di default perchè non so cos'è T
#ifndef NDEBUG
    std::cout << "cbuffer(const cbuffer &other)" << std::endl;
#endif
  }

  /** 
      Operatore di assegnamento
      @param other cbuffer da copiare
      @return Reference a this
      @throw Eccezione di allocazione di memoria
  */
  cbuffer &operator=(const cbuffer &other){
    if(this != &other){
      cbuffer tmp(other);
      this -> swap(tmp);
    }
#ifndef NDEBUG
    std::cout << "operator=(const cbuffer &other)" << std::endl;
#endif
    return *this;
  }

  /**
     Distruttore
   */
  ~cbuffer(){
    clear();
#ifndef NDEBUG
    std::cout << "~cbuffer()" << std::endl;
#endif
  }

  //COSTRUTTORI SECONDARI

  /**
     Costruttore secondario. Permette di istanziare un cbuffer con una
     determinata dimensione.
     @brief Costruttore secondario
     @param size Dimensione del cbuffer da istanziare
   */
  explicit cbuffer(size_type size): _buffer(0), _size(0), _start(0),
				    _end(0), _used(0){
    _buffer = new T[size];
    _size = size;
#ifndef NDEBUG
    std::cout << "explicit cbuffer(size_type size)" << std::endl;
#endif    
  }

  /**
     Costruttore secondario. Permette di istanziare un cbuffer con una
     determinata dimensione e di inizializzare tutti gli elementi del
     cbuffer ad il valore dato.
     @brief Costruttore secondario
     @param size Dimensione del cbuffer da istanziare
     @param el Elemento a cui inizializzare tutte le celle del cbuffer
     @throw Eccezione di allocazione di memoria
  */
  cbuffer(size_type size, const T &el): _buffer(0), _size(0), _start(0),
					_end(0), _used(0){
    _buffer = new T[size];
    _size = size;
    try{
      for(size_type i = 0; i < size; ++i){
	_buffer[i] = el;
      }
      _used = size;
    }
    catch(...){
      clear();
      throw;
    }
#ifndef NDEBUG
    std::cout << "cbuffer(size_type size, const T &el)"
	      << std::endl;
#endif 
  }

  /**
     Costrutture secondario. Costruisce il cbuffer partendo da una
     coppia di iteratori generici di una sequanza generica di dati di
     tipo non per forza conforme a quella del cbuffer. Il cast è
     lasciato, se possibile, al compilatore. Risulta inoltre
     necessario esplicitare la dimensione con cui deve essere
     istanziato il cbuffer.
     @brief Costruttore secondario.
     @param size Dimensione del cbuffer da istanziare
     @param begin Iteratore di inizio della sequenza
     @param end Iteratore di fine della sequenza
     @throw eccezione di allocazione della memoria
  */
  template <typename IterQ>
  cbuffer(size_type size, IterQ begin, IterQ end):
    _buffer(0), _size(0), _start(0), _end(0), _used(0){
    _buffer = new T[size];
    _size = size;
    try {		
      for(; begin!=end; ++begin) {
	this -> push_back(static_cast<T>(*begin));		
      }
    }
    catch(...) {
      clear();
      throw;
    }
#ifndef NDEBUG
    std::cout << "cbuffer(size_type size, IterQ begin, IterQ end)"
	      << std::endl;
#endif 
  }

  //OTHER METHODS

  /**
     Ritorna la dimensione del cbuffer
     @brief Dimensione del cbuffer.
     @return La dimensione del cbuffer
  */
  size_type size() const{
#ifndef NDEBUG
    std::cout << "size_type size() const" << std::endl;
#endif
    return _size;
  }
  
  /**
     Ritorna il numero di elementi presenti nel cbuffer.
     @brief Elementi usati nel cbuffer
     @return Il numero di elementi presenti nel cbuffer
  */
  size_type used() const{
#ifndef NDEBUG
    std::cout << "size_type used() const" << std::endl;
#endif
    return _used;
  }

  /**
     Determina se il cbuffer è vuoto oppre no.
     @return true se il cbuffer è vuoto, false altrimenti 
  */
  bool is_empty() const{
#ifndef NDEBUG
    std::cout << "bool is_empty() const" << std::endl;
#endif    
    return _used == 0;
  }

  
  /**
     Determina se il cbuffer è pieno oppre no.
     @return true se il cbuffer è pieno, false altrimenti 
  */
  bool is_full() const{
#ifndef NDEBUG
    std::cout << "bool is_full() const" << std::endl;
#endif
    return _used == _size;
  }

  /**
     Inserisce in coda al cbuffer il nuovo elemento, se il buffer è
     pieno il nuovo elemento verrà inserito nella posizione del più
     vecchio e verrà aggiornato l'elemento più vecchio, di fatto verrà
     perso l'elemento più vecchio se si effettua una push_back su un
     buffer pieno
     @brief Inserimento in coda al cbuffer.
     @param el Elemento da inserire
     @throw floating point exception Nel caso in cui si provi ad
     inserire in un cbuffer con size = 0
  */
  void push_back(const T &el){
    if(is_full()){ // se è pieno devo muovere start di una posizione. 
      _buffer[_end] = el;
      _start = (++_start)%_size;
      _end = _start;
    }
    else{
      _buffer[_end] = el;
      _end = (++_end)%_size;
      ++_used;
    }
#ifndef NDEBUG
    std::cout << "void push_back(const T &el)" << std::endl;
#endif    
  }

  /**
     Elimina l'elemento più vecchio dal cbuffer
     @thorw floating point exception Nel caso in cui si provi ad
     eliminare in un cbuffer con size = 0
  */
  void erase(){
    if(!is_empty()){
      _start = (++_start)%_size;
      --_used;
    }
    //Se è vuoto non fa nulla
#ifndef NDEBUG
    std::cout << "void erase()" << std::endl;
#endif    
  }

  /**
     Permette di leggere e scrivere l'elemento in posizione
     index del cbuffer.
     @brief Accesso agli elementi in lettura e scrittura.
     @param index Indice dell'elemento a cui si vuole accedere
     @return Il valore della cella in posizione index
     @pre index deve essere minore strettamente della dimensione del
     cbuffer 
     @throw out_of_range Se il buffer è vuoto oppure se l'indice non è
     tra gli indici degli elementi presenti effettivamente nel cbuffer
  */
  T &operator[](size_type index){
    if(index < _size){
      size_type effective_pos = (index + _start)%_size;
      if(is_empty()){
	throw std::out_of_range("Empty cbuffer");
      }
      if(is_full()){
	return _buffer[effective_pos];
      }
      // |___S******E__|
      if(_start < _end && effective_pos < _end
	 && effective_pos >= _start){
	return _buffer[effective_pos];
      }
      // |***E_____S***|
      if(_start > _end &&
	 ((effective_pos >= _start && effective_pos < _size) ||
	  (effective_pos >= 0 && effective_pos < _end))){
	return _buffer[effective_pos];
      }
    }
#ifndef NDEBUG
    std::cout << "T &operator[](size_type index)" << std::endl;
#endif
    throw std::out_of_range("Index out of bound");    
  }

  /**
     Permette di leggere l'elemento in posizione index del cbuffer.
     @brief Accesso agli elementi in lettura.
     @param index Indice dell'elemento a cui si vuole accedere
     @return Il valore della cella in posizione index
     @pre index deve essere minore strettamente della dimensione del
     cbuffer 
     @throw out_of_range Se il buffer è vuoto oppure se l'indice non è
     tra gli indici degli elementi presenti effettivamente nel cbuffer
  */
  const T &operator[](size_type index) const{
    if(index < _size){
      size_type effective_pos = (index + _start)%_size;
      if(is_empty()){
	throw std::out_of_range("empty cbuffer");
      }
      if(is_full()){
	return _buffer[effective_pos];
      }
      if(_start < _end && effective_pos < _end
	 && effective_pos >= _start){
	return _buffer[effective_pos];
      }
      if(_start > _end &&
	 ((effective_pos >= _start && effective_pos < _size) ||
	  (effective_pos >= 0 && effective_pos < _end))){
	return _buffer[effective_pos];
      }
    }
#ifndef NDEBUG
    std::cout << "const T &operator[](size_type index) const"
	      << std::endl;
#endif    
    throw std::out_of_range("Index out of bound");
  }

  /**
     Scambia il contenuto di due cbuffer
     @param other cbuffer con il quale scambiare i dati
   */
  void swap(cbuffer &other){
    std::swap(other._buffer, this -> _buffer);
    std::swap(other._start, this -> _start);
    std::swap(other._end, this -> _end);
    std::swap(other._size, this -> _size);
    std::swap(other._used, this -> _used);
  }

  //ITERATORI
  class const_iterator; // forward declaration

  /**
     Iteratore al cbuffer. Si noti che per tutte le operazioni di 
     incremento e decremento nel caso in cui si esca dal cbuffer
     l'iteratore punterà ad end. Inoltre le operazioni di confronto di
     ordine ha senso farle se i due iteratori sono riferiti allo
     stesso cbuffer.
     @brief Iteratore al cbuffer.
  */
  class iterator {
    
    size_type _offset; 
    cbuffer<T>* _cb;
    
  public:
    typedef std::random_access_iterator_tag iterator_category;
    typedef T                        value_type;
    typedef ptrdiff_t                difference_type;
    typedef T*                       pointer;
    typedef T&                       reference;

    iterator(): _offset(0), _cb(0) {}
		
    iterator(const iterator &other):
      _offset(other._offset), _cb(other._cb) {}

    iterator& operator=(const iterator &other) {
      this -> _offset = other._offset;
      this -> _cb = other._cb;
      return *this;
    }

    ~iterator() {
      _offset = 0;
      _cb = 0;
    }

    // Ritorna il dato riferito dall'iteratore (dereferenziamento)
    /**
       @throw out_of_range se l'iteratore sta puntando ad una cella
       non valida del cbuffer, come nel caso di iteratore ad end. 
    */
    reference operator*() const {
      return (*_cb)[_offset];
    }

    // Ritorna il puntatore al dato riferito dall'iteratore
    /**
       @throw out_of_range se l'iteratore sta puntando ad una cella
       non valida del cbuffer, come nel caso di iteratore ad end. 
    */
    pointer operator->() const {
      return &(*_cb)[_offset];
    }

    // Operatore di accesso random, ritorna il riferimento
    // all'elemento allocato index posizioni dopo all'elemento puntato
    // da index. 
    /**
       @throw out_of_range se l'incremento/decremento porta ad una
       posizione non valida rispetto al cbuffer. 
    */
    reference operator[](int index) {
      int pos = _offset + index;
      if(pos < 0){
	pos = _cb -> _used;
      }
      //se è positivo e >= size ci penserà l'operatore [] a
      //gestirlo. O anche se è nella parte di cbuffer non usato 
      return (*_cb)[pos];
    }
		
    // Operatore di iterazione post-incremento
    iterator operator++(int) {
      iterator tmp(*this);
      ++_offset;
      //li devo fare per operatore differenza
      if(_offset > _cb -> _used){
	_offset = _cb -> _used;
      }
      return tmp;
    }

    // Operatore di iterazione pre-incremento
    iterator &operator++() {
      ++_offset;
      if(_offset > _cb -> _used){
	_offset = _cb -> _used;
      }
      return *this;
    }

    // Operatore di iterazione post-decremento
    iterator operator--(int) {
      iterator tmp(*this);
      //devo garantirmi che se decremento un iteratore al primo
      //elemento vada alla fine
      if(_offset == 0){
	_offset = _cb -> _used;
      }
      else{
	--_offset;
      }
      return tmp;
    }

    // Operatore di iterazione pre-decremento
    iterator &operator--() {
      //discorso analogo a prima
      if(_offset == 0){
	_offset = _cb -> _used;
      }
      else{
	--_offset;
      }
      return *this;
    }

    // Spostamentio in avanti della posizione
    iterator operator+(int offset) {
      //dato che offset è int posso avere valori negativi e quindi
      //devo considerare il fatto di "andare prima di start"
      iterator tmp(*this);
      int pos = tmp._offset + offset;
      if(pos < 0 || pos > (_cb -> _used)){
	tmp._offset = _cb -> _used;
      }
      else{
	tmp._offset = pos;
      }
      return tmp;
    }

    // Spostamentio all'indietro della posizione
    iterator operator-(int offset) {
      iterator tmp(*this);
      int pos = tmp._offset - offset;
      if(pos < 0 || pos > (_cb -> _used)){
	tmp._offset = _cb -> _used;
      }
      else{
	tmp._offset = pos;
      }
      return tmp;
    }
		
    // Spostamentio in avanti della posizione
    iterator& operator+=(int offset) {
      int pos = _offset + offset;
      if(pos < 0 || pos > (_cb -> _used)){
	_offset = _cb -> _used;
      }
      else{
	_offset = pos;
      }
      return *this;
    }

    // Spostamentio all'indietro della posizione
    iterator& operator-=(int offset) {
      int pos = _offset - offset;
      if(pos < 0 || pos > (_cb -> _used)){
	_offset = _cb -> _used;
      }
      else{
	_offset = pos;
      }
      return *this;
    }

    // Numero di elementi tra due iteratori
    difference_type operator-(const iterator &other) {
      return this -> _offset - other._offset;
    }
	
    bool operator==(const iterator &other) const {
      return (this -> _cb == other._cb &&
	      this -> _offset == other._offset);
    }

    bool operator!=(const iterator &other) const {
      return (this -> _cb != other._cb ||
	      this -> _offset != other._offset);
    }

    bool operator>(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset > other._offset; 
    }
		
    bool operator>=(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset >= other._offset; 
    }

    bool operator<(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset < other._offset; 
    }

    bool operator<=(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset <= other._offset; 
    }
				
    friend class const_iterator;

    bool operator==(const const_iterator &other) const {
      return (this -> _cb == other._cb &&
	      this -> _offset == other._offset);
    }

    bool operator!=(const const_iterator &other) const {
      return (this -> _cb != other._cb ||
	      this -> _offset != other._offset);
    }

    bool operator>(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset > other._offset;
    }
		
    bool operator>=(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset >= other._offset;
    }

    bool operator<(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset < other._offset;
    }
	        
    bool operator<=(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset <= other._offset;
    }

  private:
    
    friend class cbuffer; 

    iterator(const size_type offset, cbuffer<T>* const cb):
      _offset(offset), _cb(cb){}
  }; // classe iterator
	
  /**
     Ritorna l'iteratore che indica l'inizio del cbuffer.
     @return iteratore ad inizio cbuffer
  */
  iterator begin() {
    return iterator(0, this);
  }
	
  /**
     Ritorna l'iteratore che indica la fine del cbuffer. Differisce
     dall'end della classe cbuffer.
     @return iteratore a fine cbuffer
  */
  iterator end() {
    return iterator(_used, this);
  }

  /**
     const_iterator al cbuffer. Si noti che per tutte le operazioni di
     incremento e decremento nel caso in cui si esca dal cbuffer
     l'iteratore punterà ad end. Inoltre le operazioni di confronto di
     ordine ha senso farle se i due iteratori sono riferiti allo
     stesso cbuffer.
     @brief Iteratore al cbuffer.
  */
  class const_iterator {
    
    size_type _offset; 
    const cbuffer<T>* _cb;
    
  public:
    typedef std::random_access_iterator_tag iterator_category;
    typedef T                        value_type;
    typedef ptrdiff_t                difference_type;
    typedef const T*                 pointer;
    typedef const T&                 reference;

	
    const_iterator(): _offset(0), _cb(0) {}
		
    const_iterator(const const_iterator &other):
      _offset(other._offset), _cb(other._cb) {}

    const_iterator& operator=(const const_iterator &other) {
      this -> _offset = other._offset;
      this -> _cb = other._cb;
      return *this;
    }

    ~const_iterator() {
      _offset = 0;
      _cb = 0;
    }

    // Ritorna il dato riferito dall'iteratore 
    /**
       @throw out_of_range se l'iteratore sta puntando ad una cella
       non valida del cbuffer, come nel caso di iteratore ad end. 
    */
    reference operator*() const {
      return (*_cb)[_offset];
    }

    // Ritorna il puntatore al dato riferito dall'iteratore
    /**
       @throw out_of_range se l'iteratore sta puntando ad una cella
       non valida del cbuffer, come nel caso di iteratore ad end. 
    */
    pointer operator->() const {
      return &(*_cb)[_offset];
    }

    // Operatore di accesso random
    /**
       @throw out_of_range se l'incremento/decremento porta ad una
       posizione non valida rispetto al cbuffer. 
    */
    reference operator[](int index) {
      //lancia eccezioni di []
      int pos = _offset + index;
      if(pos < 0){
	pos = _cb -> _used;
      }
      return (*_cb)[pos];
    }
		
    // Operatore di iterazione post-incremento
    const_iterator operator++(int) {
      const_iterator tmp(*this);
      ++_offset;
      //li devo fare per la differenza
      if(_offset > _cb -> _used){
	_offset = _cb -> _used;
      }
      return tmp;
    }

    // Operatore di iterazione pre-incremento
    const_iterator &operator++() {
      ++_offset;
      //li devo fare per la differenza
      if(_offset > _cb -> _used){
	_offset = _cb -> _used;
      }
      return *this;
    }

    // Operatore di iterazione post-decremento
    const_iterator operator--(int) {
      const_iterator tmp(*this);
      //devo garantirmi che se decremento un iteratore al primo
      //elemento vada alla fine
      if(_offset == 0){
	_offset = _cb -> _used;
      }
      else{
	--_offset;
      }
      return tmp;
    }

    // Operatore di iterazione pre-decremento
    const_iterator &operator--() {
      if(_offset == 0){
	_offset = _cb -> _used;
      }
      else{
	--_offset;
      }
      return *this;
    }

    // Spostamentio in avanti della posizione
    const_iterator operator+(int offset) {
      const_iterator tmp(*this);
      int pos = tmp._offset + offset;
      if(pos < 0 || pos > (_cb -> _used)){
	tmp._offset = _cb -> _used;
      }
      else{
	tmp._offset = pos;
      }
      return tmp;
    }

    // Spostamentio all'indietro della posizione
    const_iterator operator-(int offset) {
      const_iterator tmp(*this);
      int pos = tmp._offset - offset;
      if(pos < 0 || pos > (_cb -> _used)){
	tmp._offset = _cb -> _used;
      }
      else{
	tmp._offset = pos;
      }
      return tmp;
    }

		
    // Spostamentio in avanti della posizione
    const_iterator& operator+=(int offset) {
      int pos = _offset + offset;
      if(pos < 0 || pos > (_cb -> _used)){
	_offset = _cb -> _used;
      }
      else{
	_offset = pos;
      }
      return *this;
    }

    // Spostamentio all'indietro della posizione
    const_iterator& operator-=(int offset) {
      int pos = _offset - offset;
      if(pos < 0 || pos > (_cb -> _used)){
	_offset = _cb -> _used;
      }
      else{
	_offset = pos;
      }
      return *this;
    }

    // Numero di elementi tra due iteratori
    difference_type operator-(const const_iterator &other) {
      return this -> _offset - other._offset;
    }
	
    bool operator==(const const_iterator &other) const {
      return (this -> _cb == other._cb &&
	      this -> _offset == other._offset);
    }

    bool operator!=(const const_iterator &other) const {
      return (this -> _cb != other._cb ||
	      this -> _offset != other._offset);
    }

    bool operator>(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset > other._offset; 
    }
		
    bool operator>=(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset >= other._offset; 
    }

    bool operator<(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset < other._offset; 
    }
		
    bool operator<=(const const_iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset <= other._offset; 
    }
		
    friend class iterator;

    bool operator==(const iterator &other) const {
      return (this -> _cb == other._cb &&
	      this -> _offset == other._offset);
    }

    bool operator!=(const iterator &other) const {
      return (this -> _cb != other._cb ||
	      this -> _offset != other._offset);
    }

    bool operator>(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset > other._offset; 
    }
		
    bool operator>=(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset >= other._offset; 
    }

    bool operator<(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset < other._offset; 
    }
		
    bool operator<=(const iterator &other) const {
      assert(this -> _cb == other._cb);
      return this -> _offset <= other._offset; 
    }

    // Costruttore di conversione iterator -> const_iterator
    const_iterator(const iterator &other):
      _offset(other._offset), _cb(other._cb) {}

    // Assegnamento di un iterator ad un const_iterator
    const_iterator &operator=(const iterator &other) {
      this -> _offset = other._offset;
      this -> _cb = other._cb;
      return *this;
    }

  private:

    friend class cbuffer;

    const_iterator(const size_type offset, const cbuffer<T>* const cb):
      _offset(offset), _cb(cb){}
		
  }; // classe const_iterator
	
  /**
     Ritorna il const_iterator che indica l'inizio del cbuffer.
     @return const_iterator ad inizio cbuffer
  */
  const_iterator begin() const {
    return const_iterator(0, this);
  }
	
  /**
     Ritorna il const_iterator che indica la fine del cbuffer.
     @return const_iterator alla fine cbuffer
  */
  const_iterator end() const {
    return const_iterator(_used, this);
    }

};
//END CBUFFER
 
/**
   Funzione che valuta un predicato unario su ogni elemento del
   cbuffer. Stampa a schermo se l'iesimo elemento del cbuffer lo
   soddisfa oppure no. Sarà true se lo soddisfa, false altrimenti.
   @brief Funzione che valuta un predicato unario su ogni elemento del
   cbuffer. 
   @param cb cbuffer su cui valutare il predicato unario
   @param p predicato unario da valutare
*/
template<typename P, typename T>
void evaluate_if(const cbuffer<T> &cb, P p){
  typename cbuffer<T>::const_iterator it, ite;
  typename cbuffer<T>::size_type pos = 0;
  //le posizioni andranno da zero a _used - 1, usare un contatore
  //esterno è immediato 
  it = cb.begin();
  ite = cb.end();
  for(; it != ite; ++it){
    bool answear = p(*it);
    if(answear){
      std::cout << "[" << pos << "]: true" << std::endl;
    }
    else{
      std::cout << "[" << pos << "]: false" << std::endl;
    }
    ++pos;
  }
}


/**
   Ridefinizione dell'operatore di stream per la stampa del cbuffer. 
   @param os oggetto stream di output
   @param cb cbuffer da stampare
   @return reference allo stream di output
*/
template <typename T>
std::ostream &operator<<(std::ostream &os, const cbuffer<T> &cb){
  typename cbuffer<T>::const_iterator i, ie;
  for(i = cb.begin(), ie = cb.end(); i != ie; ++i){
    os << *i << std::endl;
  }
  return os;
}
  

#endif
