An implementation of a templated cyclic buffer in C++. 

It also exposes a random access iterator, please note that the access through iterators is not cyclic. That decision was made in order to avoid any kind of problem caused by the use of a double index in order to handle the loop access.

Also note that this project has been developed for an university course in Italian and so all the comments are in Italian.

Finally, you can build the project using the make file, no dependecy is required. There is also a main file used for testing the templated class.
The memory usage is always correct and causes 0 errors from Valgrind executing the main.