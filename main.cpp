#include "cbuffer.hpp"
#include <list>
#include <string>
#include "voce.h"

#ifdef NDEBUG
const std::string s = std::string(4, ' ') + "Risultati: ";
// usato per fare stampe di controllo, definite in ifdef, incolonnate
#endif

/**
   Metodo per testare i vari costruttori e metodi fondamentali. Si
   utilizza un tipo T che è int per comodità di scrittura del test
   @brief Metodo per testare i vari costruttori e metodi fondamentali
*/
void test_costruttori_e_fondamentali_int(){
  std::cout << "***** Test costruttori e metodi fondamentali int *****"
	    << std::endl;
  
  cbuffer<int> cb;
  // provare ad inserire elementi in un cbuffer costruito con il
  // costruttore di default è errore poichè il buffer non è neanche
  // stato allocato e quindi si proverebbe ad inserire nella posizione
  // di memoria 0, per come è stato definito il costruttore di default

  std::cout << "Costruttore con size e copy constructor di buffer pieno"
	    << std::endl;
  cbuffer<int> cb2(5);
  std::cout << "Inserimento dei seguenti valori nell'ordine: 0, 42, 17, 14, 23"
	    << std::endl;
  cb2.push_back(0);
  cb2.push_back(42);
  cb2.push_back(17);
  cb2.push_back(13);
  cb2.push_back(23);
  std::cout << "Stampa cb2 attraverso operatore << :" << std::endl;
  std::cout << cb2 << std::endl;
  cbuffer<int> cb3(cb2);
  std::cout << "Stampa cb3,copia di cb2, attraverso operatore << :" << std::endl;
  std::cout << cb3 << std::endl;

  std::cout << "Copy constructor di buffer che non inizia a posizione 0 fisica"
	    << " e costruttore con size e valore di default" << std::endl;
  cbuffer<int> cb4(4, 42);
  cb4.erase();
  cb4.erase();
  std::cout << "Stampa cb4 attraverso operatore << :" << std::endl;
  std::cout << cb4 << std::endl;
  cbuffer<int> cb5(cb4);
  std::cout << "Stampa cb5, copia di cb4, attraverso operatore << :" << std::endl;
  std::cout << cb5 << std::endl;

  std::cout << "Operatore uguale, copio cb2" << std::endl;
  cbuffer<int> cb6;
  cb6 = cb2;
  std::cout << "Stampa cb6 attraverso operatore << :" << std::endl;
  std::cout << cb6 << std::endl;

  std::cout << "Costruttore con iteratori, sfrutto std::list" << std::endl;
  std::list<int> l(1, 42);
  l.push_front(17);
  l.push_front(13);
  l.push_front(19);
  std::cout << "l ha nell'ordine i seguenti elementi: 19, 13, 17, 42" << std::endl;
  std::list<int>::iterator i, ie;
  i = l.begin();
  ie = l.end();
  cbuffer<int> cb7(4, i, ie);
  std::cout << "Stampa cb7 attraverso operatore << :" << std::endl;
  std::cout << cb7 << std::endl;
  cbuffer<int> cb8(6, i, ie);
  std::cout << "cb8 è stato costruito come cb7 ma ha dimensioni superiori a"
	    << " a quelle della lista ci si aspetta solo gli stessi valori di"
	    << " cb7" << std::endl;
  std::cout << "Stampa cb8 attraverso operatore << :" << std::endl;
  std::cout << cb8 << std::endl;
  cbuffer<int> cb9(2, i, ie);
  std::cout << "cb9 è stato costruito come cb7 ma ha dimensioni inferiori a"
	    << " a quelle della lista, mancheranno i primi due elementi di l"
	    << std::endl;
  std::cout << "Stampa cb9 attraverso operatore << :" << std::endl;
  std::cout << cb9 << std::endl;
  std::cout << "Creazione di cbuffer con cast da double a int:" << std::endl;
  std::list<double> l2(1, 37.3);
  l2.push_front(17.2);
  l2.push_front(23.8);
  std::list<double>::iterator i2, ie2;
  i2 = l2.begin();
  ie2 = l2.end();
  cbuffer<int> cb10(5, i2, ie2);
  std::cout << "Stampa cb10 attraverso operatore << :" << std::endl;
  std::cout << cb10 << std::endl;
  // Nel caso in cui si provi a castare un tipo non castabile ad un
  // altro tipo, attraverso uno static cast vi sarà errori in
  // compilazione. 
}

/**
   Metodo per testare i vari metodi di uso su un cbuffer di interi. Si
   testano tutti i metodi ovvero: push_back, erase, [] e i vari metodi
   che restituiscono informazioni (is_full, is_empty, size e used)
   @brief Metodo per testare i vari metodi di uso su un cbuffer di
   interi.
*/
void test_uso_cbuffer_int(cbuffer<int> &cb){
  std::cout << "***** Test uso di cbuffer<int> *****" << std::endl;

  cbuffer<int>::size_type size1 = cb.size();
  assert(size1 == 10);
  cbuffer<int>::size_type used1 = cb.used();
  assert(used1 == 0);
  bool empty1 = cb.is_empty();
  assert(empty1 == true);
  bool full1 = cb.is_full();
  assert(full1 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 10, used = 0, empty = 1, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size1 << ", used = " << used1
	    << ", empty = " << empty1 << ", full = " << full1 << std::endl;
#endif
  std::cout << "Test di erase su cbuffer vuoto, ci si aspetta che non cambi nulla"
	    <<std::endl;
  cb.erase();
  size1 = cb.size();
  assert(size1 == 10);  
  used1 = cb.used();
  assert(used1 == 0);
  empty1 = cb.is_empty();
  assert(empty1 == true);
  full1 = cb.is_full();
  assert(full1 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 10, used = 0, empty = 1, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size1 << ", used = " << used1
	    << ", empty = " << empty1 << ", full = " << full1 << std::endl;
#endif
  
  // questo accesso è stato aggiunto in un secondo momento per testare
  // quando il cbuffer è vuoto senza doverlo svuotare in seguito a
  // tutti gli inserimenti, per comodità della scrittura nei test, per
  // questo si usa el6
  std::cout << "Ci si aspetta un messaggio di eccezione empty cbuffer"
	    << std::endl;
  try{
    int el6 = cb[0];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }

  std::cout << "Inserimento degli elementi 1, 2, 3, 4, 5, 6, 7, 0, 9, 42, 17, 23"
	    << std::endl;
  cb.push_back(1);
  cb.push_back(2);
  cb.push_back(3);
  cb.push_back(4);
  cb.push_back(5);
  cb.push_back(6);
  cb.push_back(7);
  cb.push_back(0);
  cb.push_back(9);
  cb.push_back(42);
  cb.push_back(17);
  cb.push_back(23);
  std::cout << "Ci si aspetta che il cbuffer contenga, nell'ordine,"
	    << " questi elementi: 3 4 5 6 7 0 9 42 17 23" << std::endl;
  std::cout << cb << std::endl;
  cbuffer<int>::size_type size2 = cb.size();
  assert(size2 == 10);  
  cbuffer<int>::size_type used2 = cb.used();
  assert(used2 == 10);
  bool empty2 = cb.is_empty();
  assert(empty2 == false);
  bool full2 = cb.is_full();
  assert(full2 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 10, used = 10, empty = 0, full = 1"
	    << std::endl;
  std::cout << s << "size = " << size2 << ", used = " << used2
	    << ", empty = " << empty2 << ", full = " << full2 << std::endl;
#endif

  std::cout << "Eliminazione dei 3 elementi più vecchi" << std::endl;
  cb.erase();
  cb.erase();
  cb.erase();
  std::cout << "Ci si aspetta che il cbuffer contenga, nell'ordine,"
	    << " questi elementi: 6 7 0 9 42 17 23" << std::endl;
  std::cout << cb << std::endl;
  cbuffer<int>::size_type size3 = cb.size();
  assert(size3 == 10);  
  cbuffer<int>::size_type used3 = cb.used();
  assert(used3 == 7);
  bool empty3 = cb.is_empty();
  assert(empty3 == false);
  bool full3 = cb.is_full();
  assert(full3 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 10, used = 7, empty = 0, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size3 << ", used = " << used3
	    << ", empty = " << empty3 << ", full = " << full3 << std::endl;
#endif
  
  std::cout << "Test di operatore []" << std::endl;
  int el = cb[0];
  assert(el == 6);
  int el2 = cb[4];
  assert(el2 == 42);
  int el3 = cb[6];
  assert(el3 == 23);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el = 6, el2 = 42, el3 = 23"
	    << std::endl;
  std::cout << s << "el = " << el << ", el2 = " << el2
	    << ", el3 = " << el3 << std::endl;
#endif
  std::cout << "Ci si aspetta due index of bound come messaggio di due tentativi"
	    << " di accesso scorretti" << std::endl;
  try{
    int el4 = cb[7];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione 1: " << e.what() << std::endl;
  }
  try{
    int el5 = cb[13];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione 2: " << e.what() << std::endl;
  }
  
  std::cout << "Prova ad assegnare dei nuovi valori ad elementi nel cbuffer."
	    << " Cambio il valore in posizione 3, il quarto elemento"
	    << std::endl;
  cb[3] = 3;
  int el7 = cb[3];
  assert(el7 == 3);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el7 = 3" << std::endl;
  std::cout << s << "el7 = " << el7 << std::endl;
#endif
  std::cout << "Alla fine di questi test il buffer è costituito da: "
	    << std::endl;
  std::cout << cb << std::endl;
}

/**
   Metodo per il test su un cbuffer costante. Si è provato ad usare in
   modo scorretto delle operazioni per modificare i dati ed il
   compilatore ha dato i corretti errori.
   @brief Metodo per il test su un cbuffer costante.
*/
void test_const_cbuffer_int(const cbuffer<int> &cb){
  std::cout << "***** Test su cbuffer<int> costante *****"
	    << std::endl; 

  std::cout << "Test metodi che danno informazioni:" << std::endl;
  cbuffer<int>::size_type size1 = cb.size();
  assert(size1 == 10);
  cbuffer<int>::size_type used1 = cb.used();
  assert(used1 == 7);
  bool empty1 = cb.is_empty();
  assert(empty1 == false);
  bool full1 = cb.is_full();
  assert(full1 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 10, used = 7, empty = 0, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size1 << ", used = " << used1
	    << ", empty = " << empty1 << ", full = " << full1 << std::endl;
#endif
  // i metodi erase e push_back non possono essere
  // invocati poichè non const  

  std::cout << "Test di operatore []" << std::endl;
  int el = cb[1];
  assert(el == 7);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el = 7"
	    << std::endl;
  std::cout << s << "el = " << el << std::endl;
#endif
  std::cout << "Ci si aspetta index of bound come messaggio di"
	    << " accesso scorretto" << std::endl;
  try{
    int el4 = cb[-1];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }
  // il metodo swap e usare l'operatore [] per assegnare un nuovo
  // valore ad una cella di memoria non possono essere usati poichè
  // anche questi sono operazioni che violano la const
}

/**
   Funtore per stabilire se un intero è uguale a 42 oppure no. Ritorna
   true se x == 42, false altrimenti.
   @brief Funtore per stabilire se un intero è uguale a 42 oppure no.
*/
struct equal_42{
  bool operator()(const int x) const{
    return x == 42;
  } 
};

/**
   Funtore per stabilire se un intero è maggiore di 5. Ritorna
   true se x > 5, false altrimenti.
   @brief Funtore per stabilire se un intero è maggiore di 5
*/
struct greater_than_5{
  bool operator()(const int x) const{
    return x > 5;
  }
};

/**
   Funtore per stabilire se un intero è pari. Ritorna
   true se x è pari, false altrimenti. Lo zero è considerato pari 
   @brief Funtore per stabilire se un intero è pari.
*/
struct number_even{
  bool operator()(const int x) const{
    return x%2 == 0;
  }
};

/**
   Metodo per testare evaluate_if sugli interi. Sono stati usate le
   tre strutture definite nel main.cpp specializzate sugli interi.
   @brief Metodo per testare evaluate_if sugli interi.
*/
void test_evaluate_if_int(const cbuffer<int> &cb){
  std::cout << "***** Test di evaluate if con vari predicati unari"
	    << " su interi *****" << std::endl;

  equal_42 p;
  greater_than_5 p2;
  number_even p3;

  std::cout << "Predicato equal_42:" << std::endl;
  evaluate_if(cb, p);

  std::cout << "Predicato greater_than_5:" << std::endl;
  evaluate_if(cb, p2);

  std::cout << "Predicato number_even:" << std::endl;
  evaluate_if(cb, p3);
}

/**
   Metodo per testare gli iterator. Sono state effettuate tutte le
   possibili operazioni tranne l'operatore -> che è stato testato solo
   quando sono stati testati i cbuffer con la classe voce.
   @brief Metodo per testare gli iterator.
*/
void test_iteratori_int(cbuffer<int> &cb){
  std::cout << "***** Test iteratori non const sul cbuffer"
	    << " utilizzato per gli ultimi test *****" << std::endl;

  std::cout << "Test costruttore di default e operatore *"
	    << std::endl;
  typename cbuffer<int>::iterator i;
  i = cb.begin();
  int el = *i;
  assert(el == 6);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el = 6"
	    << std::endl;
  std::cout << s << "el = " << el << std::endl;
#endif

  std::cout << "Test costruttore per copia e pre-incremento"
	    << std::endl;
  typename cbuffer<int>::iterator i2(i);
  ++i2;
  int el2 = *i2;
  assert(el2 == 7);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el2 = 7"
	    << std::endl;
  std::cout << s << "el2 = " << el2 << std::endl;
#endif

  std::cout << "Test per assegnamento = tra iterator"
	    <<std::endl;
  typename cbuffer<int>::iterator i3;
  i3 = i2;
  int el3 = *i3;
  assert(el2 == 7);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el3 = 7"
	    << std::endl;
  std::cout << s << "el3 = " << el2 << std::endl;
#endif

  std::cout << "Test per operatore []:" << std::endl;
  std::cout << "da un iteratore che coincide con begin:" << std::endl;
  int el4 = i[0];
  assert(el4 == 6);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el4 = 6"
	    << std::endl;
  std::cout << s << "el4 = " << el4 << std::endl;
#endif
  //utilizzo i2 per verificare se "parte a contare" le posizioni dalla
  //posizione di start del cbuffer oppure da dove indica l'iteratore
  std::cout << "da un iteratore che indica il secondo elemento del cbuffer"
	    << std::endl;
  int el5 = i2[4];
  assert(el5 == 17);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el5 = 17"
	    << std::endl;
  std::cout << s << "el5 = " << el5 << std::endl;
#endif
  std::cout << "Ci si aspetta un index out of bound come messaggio di"
	    << " accesso scorretto" << std::endl;
  try{
    int el6 = i[8];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }

  std::cout << "Test operatori di incremento e decremento. Si sfrutta"
	    << " un iteratore al primo elemento" << std::endl;
  int el7 = *(i++);
  assert(el7 == 6);
  int el8 = *i;
  assert(el8 == 7);
  ++i;
  int el9 = *i;
  assert(el9 == 0);
  i += 2;
  int el10 = *i;
  assert(el10 == 42);
  int el11 = *(i--);
  assert(el11 == 42);
  int el12 = *i;
  assert(el12 == 3);
  --i;
  int el13 = *i;
  assert(el13 == 0);
  i = i2 + 5;
  int el14 = *i;
  assert(el14 == 23);
  i -= 3;
  int el15 = *i;
  assert(el15 == 3);
  i = i - (-1);
  int el16 = *i;
  assert(el16 == 42);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el7 = 6, el8 = 7, el9 = 0, el10 = 42, el11 = 42"
	    << ", el12 = 3, el13 = 0, el14 = 23, el15 = 3, el16 = 42" << std::endl;
  std::cout << s << "el7 = " << el7  << ", el8 = " << el8  << ", el9 = " << el9
	    << ", el10 = " << el10  << ", el11 = " << el11  << ", el12 = " << el12
	    << ", el13 = " << el13  << ", el14 = " << el14  << ", el15 = " << el15
	    << ", el16 = " << el16 << std::endl;
#endif
  
  std::cout << "Test - tra iteratori" << std::endl;
  i = cb.begin();
  i2 = cb.end();
  int diff1 = i2 - i;
  assert(diff1 == 7);
  i += 2;
  i2 -= 1;
  int diff2 = i2 - i;
  assert(diff2 == 4);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: diff1 = 7, diff2 = 4" << std::endl;
  std::cout << s << "diff1 = " << diff1 << ", diff2 = " << diff2
	    << std::endl;
#endif

  std::cout << "Test operatori di confronto tra iteratori" << std::endl;
  i = cb.begin();
  i2 = cb.begin();
  bool equal1 = (i == i2);
  assert(equal1 == true);
  bool nequal1 = (i != i2);
  assert(nequal1 == false);
  ++i2;
  bool equal2 =(i == i2);
  assert(equal2 == false);
  bool nequal2 = (i != i2);
  assert(nequal2 == true);
  cbuffer<int> cb2(2, 1);
  i2 = cb2.begin();
  bool equal3 = (i == i2);
  assert(equal3 == false);
  bool nequal3 = (i != i2);
  assert(nequal3 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: equal1 = 1, nequal1 = 0, equal2 = 0, nequal2 = 1,"
	    << " equal3 = 0, nequal3 = 1" << std::endl;
  std::cout << s << "equal1 = " << equal1 << ", nequal1 = " << nequal1
	    << ", equal2 = " << equal2 << ", nequal2 = " << nequal2
	    << ", equal3 = " << equal3 << ", nequal3 = " << nequal3
	    << std::endl;
#endif
  i = cb.begin();
  i2 = cb.end();
  i3 = cb.begin();
  i2 -= 2;
  bool c1 = (i2 > i);
  assert(c1 == true);
  bool c2 = (i >= i3); //voglio che siano uguali
  assert(c2 == true);
  ++i; //lo sposto per vedere se per caso succede qualcosa di strano 
  bool c3 = i2 < i;
  assert(c3 == false);
  bool c4 = i <= i2;
  assert(c4 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: c1 = 1, c2 = 1, c3 = 0, c4 = 1"
	    << std::endl;
  std::cout << s << "c1 = " << c1 << ", c2 = " << c2
	    << ", c3 = " << c3 << ", c4 = " << c4
	    << std::endl;
#endif
  
  typename cbuffer<int>::const_iterator ci;
  i = cb.begin();
  ci = cb.begin();
  bool equal4 = (i == ci);
  assert(equal4 == true);
  bool nequal4 = (i != ci);
  assert(nequal4 == false);
  ++ci;
  bool equal5 =(i == ci);
  assert(equal5 == false);
  bool nequal5 = (i != ci);
  assert(nequal5 == true);
  cbuffer<int> cb3(2, 1);
  ci = cb2.begin();
  bool equal6 = (i == ci);
  assert(equal6 == false);
  bool nequal6 = (i != ci);
  assert(nequal6 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: equal4 = 1, nequal4 = 0, equal5 = 0, nequal5 = 1,"
	    << " equal6 = 0, nequal6 = 1" << std::endl;
  std::cout << s << "equal4 = " << equal4 << ", nequal4 = " << nequal4
	    << ", equal5 = " << equal5 << ", nequal5 = " << nequal5
	    << ", equal6 = " << equal6 << ", nequal6 = " << nequal6
	    << std::endl;
#endif
  i = cb.begin();
  ci = cb.end();
  ci -= 2;
  ++i;
  bool c5 = (i > ci);
  assert(c5 == false);
  bool c6 = (i >= ci);
  assert(c6 == false);
  bool c7 = (i < ci);
  assert(c7 == true);
  bool c8 = (i <= ci);
  assert(c8 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: c5 = 0, c6 = 0, c7 = 1, c8 = 1"
	    << std::endl;
  std::cout << s << "c5 = " << c5 << ", c6 = " << c6
	    << ", c7 = " << c7 << ", c8 = " << c8
	    << std::endl;
#endif

  std::cout << "Test di modifica di cbuffer usando iterator, * e []."
	    << " Un buffer allocato per altri test all'interno"
	    << " del metodo per questi test" << std::endl;
  i = cb3.begin();
  *i = 101;
  i[1] = 16;
  std::cout << "Ci si aspetta 101 e 16 come valori nel cbuffer."
	    << std::endl << "Valori cbuffer:" << std::endl;
  std::cout << cb3 << std::endl;
}

/**
   Metodo per testare i const_iterator. Sono state effettuate tutte le
   possibili operazioni tranne l'operatore -> che è stato testato solo
   quando sono stati testati i cbuffer con la classe voce.
   @brief Metodo per testare i const_iterator.
*/
void test_iteratori_costanti_int(cbuffer<int> &cb){
  std::cout << "***** Test iteratori const sul cbuffer"
	    << " utilizzato per gli ultimi test *****" << std::endl;
  std::cout << "Il test precedente non ne ha modificato il contenuto"
	    << " quindi ci si aspettano gli stessi risultati"
	    << " eseguendo gli stessi test." << std::endl;
  
  std::cout << "Test costruttore di default e operatore *"
	    << std::endl;
  typename cbuffer<int>::const_iterator i;
  i = cb.begin();
  int el = *i;
  assert(el == 6);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el = 6"
	    << std::endl;
  std::cout << s << "el = " << el << std::endl;
#endif

  std::cout << "Test costruttore per copia e pre-incremento"
	    << std::endl;
  typename cbuffer<int>::const_iterator i2(i);
  ++i2;
  int el2 = *i2;
  assert(el2 == 7);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el2 = 7"
	    << std::endl;
  std::cout << s << "el2 = " << el2 << std::endl;
#endif

  std::cout << "Test per assegnamento = tra iterator"
	    <<std::endl;
  typename cbuffer<int>::const_iterator i3;
  i3 = i2;
  int el3 = *i3;
  assert(el2 == 7);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el3 = 7"
	    << std::endl;
  std::cout << s << "el3 = " << el2 << std::endl;
#endif

  std::cout << "Test per operatore []:" << std::endl;
  std::cout << "da un iteratore che coincide con begin:" << std::endl;
  int el4 = i[0];
  assert(el4 == 6);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el4 = 6"
	    << std::endl;
  std::cout << s << "el4 = " << el4 << std::endl;
#endif
  //utilizzo i2 per lo stesso motivo per cui l'ho usato nel test sugli
  //iteratori 
  std::cout << "da un iteratore che indica il secondo elemento del cbuffer"
	    << std::endl;
  int el5 = i2[4];
  assert(el5 == 17);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el5 = 17"
	    << std::endl;
  std::cout << s << "el5 = " << el5 << std::endl;
#endif
  std::cout << "Ci si aspetta un index out of bound come messaggio di"
	    << " accesso scorretto" << std::endl;
  try{
    int el6 = i[-22];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }

  std::cout << "Test operatori di incremento e decremento. Si sfrutta"
	    << " un iteratore al primo elemento" << std::endl;
  int el7 = *(i++);
  assert(el7 == 6);
  int el8 = *i;
  assert(el8 == 7);
  ++i;
  int el9 = *i;
  assert(el9 == 0);
  i += 2;
  int el10 = *i;
  assert(el10 == 42);
  int el11 = *(i--);
  assert(el11 == 42);
  int el12 = *i;
  assert(el12 == 3);
  --i;
  int el13 = *i;
  assert(el13 == 0);
  i = i2 + 5;
  int el14 = *i;
  assert(el14 == 23);
  i -= 3;
  int el15 = *i;
  assert(el15 == 3);
  i = i - (-1);
  int el16 = *i;
  assert(el16 == 42);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el7 = 6, el8 = 7, el9 = 0, el10 = 42, el11 = 42"
	    << ", el12 = 3, el13 = 0, el14 = 23, el15 = 3, el16 = 42" << std::endl;
  std::cout << s << "el7 = " << el7  << ", el8 = " << el8  << ", el9 = " << el9
	    << ", el10 = " << el10  << ", el11 = " << el11  << ", el12 = " << el12
	    << ", el13 = " << el13  << ", el14 = " << el14  << ", el15 = " << el15
	    << ", el16 = " << el16 << std::endl;
#endif
  
  std::cout << "Test - tra iteratori" << std::endl;
  i = cb.begin();
  i2 = cb.end();
  int diff1 = i2 - i;
  assert(diff1 == 7);
  i += 2;
  i2 -= 1;
  int diff2 = i2 - i;
  assert(diff2 == 4);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: diff1 = 7, diff2 = 4" << std::endl;
  std::cout << s << "diff1 = " << diff1 << ", diff2 = " << diff2
	    << std::endl;
#endif

  std::cout << "Test operatori di confronto tra iteratori" << std::endl;
  i = cb.begin();
  i2 = cb.begin();
  bool equal1 = (i == i2);
  assert(equal1 == true);
  bool nequal1 = (i != i2);
  assert(nequal1 == false);
  ++i2;
  bool equal2 =(i == i2);
  assert(equal2 == false);
  bool nequal2 = (i != i2);
  assert(nequal2 == true);
  cbuffer<int> cb2(2, 1);
  i2 = cb2.begin();
  bool equal3 = (i == i2);
  assert(equal3 == false);
  bool nequal3 = (i != i2);
  assert(nequal3 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: equal1 = 1, nequal1 = 0, equal2 = 0, nequal2 = 1,"
	    << " equal3 = 0, nequal3 = 1" << std::endl;
  std::cout << s << "equal1 = " << equal1 << ", nequal1 = " << nequal1
	    << ", equal2 = " << equal2 << ", nequal2 = " << nequal2
	    << ", equal3 = " << equal3 << ", nequal3 = " << nequal3
	    << std::endl;
#endif
  i = cb.begin();
  i2 = cb.end();
  i3 = cb.begin();
  i2 -= 2;
  bool c1 = (i2 > i);
  assert(c1 == true);
  bool c2 = (i >= i3); //voglio che siano uguali
  assert(c2 == true);
  ++i; //lo sposto per vedere se per caso succede qualcosa di strano 
  bool c3 = i2 < i;
  assert(c3 == false);
  bool c4 = i <= i2;
  assert(c4 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: c1 = 1, c2 = 1, c3 = 0, c4 = 1"
	    << std::endl;
  std::cout << s << "c1 = " << c1 << ", c2 = " << c2
	    << ", c3 = " << c3 << ", c4 = " << c4
	    << std::endl;
#endif
  
  typename cbuffer<int>::iterator i4;
  i = cb.begin();
  i4 = cb.begin();
  bool equal4 = (i == i4);
  assert(equal4 == true);
  bool nequal4 = (i != i4);
  assert(nequal4 == false);
  ++i4;
  bool equal5 =(i == i4);
  assert(equal5 == false);
  bool nequal5 = (i != i4);
  assert(nequal5 == true);
  cbuffer<int> cb3(2, 1);
  i4 = cb2.begin();
  bool equal6 = (i == i4);
  assert(equal6 == false);
  bool nequal6 = (i != i4);
  assert(nequal6 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: equal4 = 1, nequal4 = 0, equal5 = 0, nequal5 = 1,"
	    << " equal6 = 0, nequal6 = 1" << std::endl;
  std::cout << s << "equal4 = " << equal4 << ", nequal4 = " << nequal4
	    << ", equal5 = " << equal5 << ", nequal5 = " << nequal5
	    << ", equal6 = " << equal6 << ", nequal6 = " << nequal6
	    << std::endl;
#endif
  i = cb.begin();
  i4 = cb.end();
  i4 -= 2;
  ++i;
  bool c5 = (i > i4);
  assert(c5 == false);
  bool c6 = (i >= i4);
  assert(c6 == false);
  bool c7 = (i < i4);
  assert(c7 == true);
  bool c8 = (i <= i4);
  assert(c8 == true);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: c5 = 0, c6 = 0, c7 = 1, c8 = 1"
	    << std::endl;
  std::cout << s << "c5 = " << c5 << ", c6 = " << c6
	    << ", c7 = " << c7 << ", c8 = " << c8
	    << std::endl;
#endif

  // Essendo un const_iterator non si possono effettuare modifiche
  // agli elementi nè tramite l'operatore * nè attraverso [] poichè,
  // altrimenti ci sarebbero errori in compilazione.

  std::cout << "Test costruttore di conversione a const_iterator"
	    << " da iterator e assegnamento = a const_iterator da"
	    << " iterator" << std::endl;
  typename cbuffer<int>::const_iterator i5(i4); // i4 è un iterator
  int el17 = *i5;
  assert(el17 == 17);
  i4 -= -1; // si sposta avanti di un elemento
  i5 = i4;
  int el18 = *i5;
  assert(el18 == 23);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el17 = 17, el18 = 23" << std::endl;
  std::cout << s << "el17 = " << el17 << ", el18 = " << el18
	    <<std::endl;
#endif
}

/**
   Funtore per stabilire se una stringa è più lunga di 7
   caratteri. Ritorna true se la stringa ha lunghezza > 7. Per stringa
   si intendono quelle implementate nella STL.
   @brief Funtore per stabilire se una stringa è più lunga di 7
   caratteri. 
*/
struct size_greater_7{
  bool operator()(const std::string &x){
    return (x.size() > 7);
  }
};

/**
   Funtore per stabilire se una stringa è uguale alla stringa
   'PAPEROGA'. Ritorna true se la stringa è uguale a PAPEROGA
   indipendentemente dai caratteri maiuscoli o minuscoli della stringa 
   in input. Ci si riferisce a stringhe della STL.
   @brief Funtore per stabilire se una stringa è uguale alla stringa
   'PAPEROGA'.
*/
struct is_paperoga{
  bool operator()(const std::string &x){
    std::string ux = "";
    for (int i = 0; i < x.size(); ++i) {
      char c = std::toupper(x[i]);
      ux = ux + c;
    }
    return ux.compare("PAPEROGA") == 0;
  }
};

/**
   Metodo usato per testare cbuffer contenente stringhe della
   STL. Sono stati fatti una serie di test significativi comprendendo
   un paio di casi non testati precedentemente. Non si testa ogni
   singolo caso poichè già testati in precedenza.
   @brief Metodo usato per testare cbuffer contenente stringhe della STL.
*/
void test_stringhe(){
  std::cout << "***** Test su cbuffer di stringhe *****" << std::endl;
  
  std::string a[6] = {"Paperino", "Paperone", "Clarabella",
		      "Eta Beta", "Minni", "Petulia"};
  cbuffer<std::string> cb(6, a, a + 6);
  std::cout << "cb dovrebbe essere composto dai seguenti elementi"
	    <<": 'Paperino' 'Paperone' 'Clarabella' 'Eta Beta'"
	    << " 'Minni' 'Petulia'" << std::endl;
  std::cout << "cb è effettivamente composto da:" << std::endl
	    << cb;
  
  std::cout << "3 cancallazioni e poi due inserimenti eseguiti, "
	    << "sono stati inseriti 'Gastone' e 'Paperoga' "
	    << "nell'ordine" << std::endl;
  cb.erase();
  cb.erase();
  cb.erase();
  cb.push_back("Gastone");
  cb.push_back("Paperoga");
  cbuffer<std::string>::size_type size1 = cb.size();
  assert(size1 == 6);
  cbuffer<std::string>::size_type used1 = cb.used();
  assert(used1 == 5);
  bool empty1 = cb.is_empty();
  assert(empty1 == false);
  bool full1 = cb.is_full();
  assert(full1 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 6, used = 5, empty = 0, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size1 << ", used = " << used1
	    << ", empty = " << empty1 << ", full = " << full1 << std::endl;
#endif
  std::cout << "cb è effettivamente composto da:" << std::endl
	    << cb;

  std::cout << "Tentativi di accesso e di modifica sfruttando il ritorno"
	    << " del reference da 'Gastone' a 'Archimede'" << std::endl;
  std::string &el1 = cb[3];
  assert(el1 == "Gastone");
  //lo devo stampare qui perchè dopo el1 prenderà il valore di
  //Archimede perchè è un reference
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el1 = Gastone"
	    << std::endl;
  std::cout << s << "el1 = " << el1 << std::endl;
#endif
  el1 = "Archimede";
  std::string el2 = cb[3];
  assert(el2 == "Archimede");
#ifdef NDEBUG
  std::cout << "Ci si aspetta: el2 = Archimede"
	    << std::endl;
  std::cout << s << "el2 = " << el2 << std::endl;
#endif
  std::cout << "Ci si aspetta index of bound come messaggio di"
	    << " accesso scorretto" << std::endl;
  try{
    std::string el3 = cb[5];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }
  std::cout << "cb è effettivamente composto da:" << std::endl
	    << cb;

  std::cout << "Test di evaluate_if su stringhe:" << std::endl;
  size_greater_7 p;
  is_paperoga p2;

  std::cout << "size_greater_7:" << std::endl;
  evaluate_if(cb, p);

  std::cout << "is_paperoga:" << std::endl;
  evaluate_if(cb, p2);
}

/**
   Funtore per stabilire se una una voce ha un cognome composto. Per
   cognome composto si intende cognome composto da almeno due parole
   separate da spazio. Esempio: van Beethoven è composto, Bevilacqua
   no. Ritorno true se è composto, false altrimenti.
   @brief Funtore per stabilire se una una voce ha un cognome composto.
*/
struct has_composed_surname{
  bool operator()(const voce &x){
    for(int i = 0; i < x.cognome.size(); ++i){
      if(x.cognome[i] == ' '){
	return true;
      }
    }
    return false;
  }
};

/**
   Metodo usato per testare cbuffer contenente oggetti della classe
   voce. Vi sono un paio di test di casisistiche non testati
   precedentemente e il test sull'operatore -> degli iteratori.
   @brief Metodo usato per testare cbuffer contenente oggetti della 
   classe voce.
*/
void test_voce(){
  std::cout << "***** Test su cbuffer di struct voce *****" << std::endl;
  std::cout << "Non sono state utilizzate le assert in questo test, è necessario"
	    << " un controllo manuale" << std::endl;
  
  std::cout << "Test di costruzione" << std::endl;
  cbuffer<voce> cb(5);
  voce v("Mozart", "Wolgang Amadeus", "12345678");
  cb.push_back(v);
  voce v2("van Beethoven", "Ludwig", "87654321");
  cb.push_back(v2);
  voce v3("Vivaldi", "Antonio", "55553334");
  cb.push_back(v3);
  cb.erase();
  voce v4("Bach", "Johann Sebastian", "098765432");
  cb.push_back(v4);
  std::cout << "cb è composto da:" << std::endl
	    << cb << std::endl;
  cbuffer<voce>::size_type size1 = cb.size();
  assert(size1 == 5);
  cbuffer<voce>::size_type used1 = cb.used();
  assert(used1 == 3);
  bool empty1 = cb.is_empty();
  assert(empty1 == false);
  bool full1 = cb.is_full();
  assert(full1 == false);
#ifdef NDEBUG
  std::cout << "Ci si aspetta: size = 5, used = 3, empty = 0, full = 0"
	    << std::endl;
  std::cout << s << "size = " << size1 << ", used = " << used1
	    << ", empty = " << empty1 << ", full = " << full1 << std::endl;
#endif

  std::cout << "Test di accesso con operatore []" << std::endl;
  const voce &el1 = cb[1];
  std::cout << "Ci si aspetta che sia la voce di Vivaldi" << std::endl;
  std::cout << el1 << std::endl;
  // ad el1 non si possono assegnare delle altre voci poichè è un
  // reference costante  
  std::cout << "Ci si aspetta index of bound come messaggio di"
	    << " accesso scorretto" << std::endl;
  try{
    voce el2 = cb[-1];
  }
  catch(const std::out_of_range &e){
    std::cout << "Messaggio dell'eccezione: " << e.what() << std::endl;
  }

  std::cout << "Test iterator con operatore ->, *, [] e typedef" << std::endl;
  typename cbuffer<voce>::iterator i;
  i = cb.begin();
  i += 2;
  // veloce test di utilizzo sfruttando i typedef
  typename cbuffer<voce>::iterator::reference el3 = *i;
  voce v5("Chopin", "Fryderyk", "13374223");
  el3 = v5;
  std::cout << "Ci si aspetta Chopin al posto di Bach" << std::endl;
  std::cout << "Stampa di cb:" << std::endl << cb << std::endl;
  std::string cognome = i -> cognome;
  std::cout << "Ci si asetta che il cognome sia Chopin" << std::endl;
  std::cout << "Il cognome effettivo è " << cognome << std::endl;
  voce el4 = i[-1];
  std::cout << "Ci si aspetta la voce di Vivaldi" << std::endl;
  std::cout << el4 << std::endl;

  std::cout << "Test di evaluate_if su voce:" << std::endl;
  has_composed_surname p;
  std::cout << "has_composed_surname:" << std::endl;
  evaluate_if(cb, p);
}

int main(){

  test_costruttori_e_fondamentali_int();
  
  cbuffer<int> cb(10);
  test_uso_cbuffer_int(cb);
  
  test_const_cbuffer_int(cb);
  std::cout << std::endl;
  
  test_evaluate_if_int(cb);
  std::cout << std::endl;

  test_iteratori_int(cb);

  test_iteratori_costanti_int(cb);
  std::cout << std::endl;
  
  test_stringhe();
  std::cout << std::endl;
  
  test_voce();
  
  return 0;
}
